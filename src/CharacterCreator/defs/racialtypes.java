/*
 * classes.java
 *
 * Created on November 25, 2003, 2:04 AM
 */

package CharacterCreator.defs;

/**
 *
 * @author  James
 */
public class racialtypes {
    public racialtypes() {
    }
    
    public static int Label = 1;
    public static int Abrev = 2;
    public static int Name = 3;
    public static int ConverName = 4;
    public static int ConverNameLower = 5;
    public static int NamePlural = 6;
    public static int Description = 7;
    public static int Appearance = 8;
    public static int StrAdjust = 9;
    public static int DexAdjust = 10;
    public static int IntAdjust = 11;
    public static int ChaAdjust = 12;
    public static int WisAdjust = 13;
    public static int ConAdjust = 14;
    public static int Endurance = 15;
    public static int Favored = 16;
    public static int FeatsTable = 17;
    public static int Biography = 18;
    public static int PlayerRace = 19;
    public static int Constant = 20;
    public static int AGE = 21;
    public static int ToolsetDefaultClass = 22;
    public static int CRModifier = 23;
    
    
}


