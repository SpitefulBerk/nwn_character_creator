/*
 * classes.java
 *
 * Created on November 25, 2003, 2:04 AM
 */

package CharacterCreator.defs;

/**
 *
 * @author  James
 */
public class classes {
    public classes() {
    }
    
	public static int Index = 0;
    public static int Label = 1;
    public static int Name = 2;
    public static int Plural = 3;
    public static int Lower = 4;
    public static int Description = 5;
    public static int Icon = 6;
    public static int HitDie = 7;
    public static int AttackBonusTable = 8;
    public static int FeatsTable = 9;
    public static int SavingThrowTable = 10;
    public static int SkillsTable = 11;
    public static int BonusFeatsTable = 12; //ADDED - HOTU
    public static int SkillPointBase = 13;
    public static int SpellGainTable = 14;
    public static int SpellKnownTable = 15;
    public static int PlayerClass = 16;
    public static int SpellCaster = 17;
    public static int Str = 18;
    public static int Dex = 19;
    public static int Con = 20;
    public static int Wis = 21;
    public static int Int = 22;
    public static int Cha = 23;
    public static int PrimaryAbil = 24;
    public static int AlignRestrict = 25;
    public static int AlignRstrctType = 26;
    public static int InvertRestrict = 27;
    public static int Constant = 28;
    public static int EffCRLvl01 = 29;
    public static int EffCRLvl02 = 30;
    public static int EffCRLvl03 = 31;
    public static int EffCRLvl04 = 32;
    public static int EffCRLvl05 = 33;
    public static int EffCRLvl06 = 34;
    public static int EffCRLvl07 = 35;
    public static int EffCRLvl08 = 36;
    public static int EffCRLvl09 = 37;
    public static int EffCRLvl10 = 38;
    public static int EffCRLvl11 = 39;
    public static int EffCRLvl12 = 40;
    public static int EffCRLvl13 = 41;
    public static int EffCRLvl14 = 42;
    public static int EffCRLvl15 = 43;
    public static int EffCRLvl16 = 44;
    public static int EffCRLvl17 = 45;
    public static int EffCRLvl18 = 46;
    public static int EffCRLvl19 = 47;
    public static int EffCRLvl20 = 48;
    public static int PreReqTable = 49;
    public static int MaxLevel = 50;
    public static int XPPenalty = 51;
    public static int ArcSpellLvlMod = 52; //ADDED - HOTU
    public static int DivSpellLvlMod = 53; //ADDED - HOTU
    public static int EpicLevel = 54; //ADDED - HOTU
    public static int Package = 55;
    
    
}


