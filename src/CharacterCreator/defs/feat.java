/*
 * classes.java
 *
 * Created on November 25, 2003, 2:04 AM
 */

package CharacterCreator.defs;

/**
 *
 * @author  James
 */
public class feat {
    public feat() {
    }
    
    public static int LABEL = 1;
    public static int FEAT = 2;
    public static int DESCRIPTION = 3;
    public static int ICON = 4;
    public static int MINATTACKBONUS = 5;
    public static int MINSTR = 6;
    public static int MINDEX = 7;
    public static int MININT = 8;
    public static int MINWIS = 9;
    public static int MINCON = 10;
    public static int MINCHA = 11;
    public static int MINSPELLLEVEL = 12; 
    public static int PREREQFEAT1 = 13;
    public static int PREREQFEAT2 = 14;
    public static int GAINMULTIPLE = 15;
    public static int EFFECTSSTACK = 16;
    public static int ALLCLASSESCANUSE = 17;
    public static int CATEGORY = 18;
    public static int MAXCR = 19;
    public static int SPELLID = 20;
    public static int SUCCESSOR = 21;
    public static int CRValue = 22;
    public static int USESPERDAY = 23;
    public static int MASTERFEAT = 24;
    public static int TARGETSELF = 25;
    public static int OrReqFeat0 = 26;
    public static int OrReqFeat1 = 27;
    public static int OrReqFeat2 = 28;
    public static int OrReqFeat3 = 29;
    public static int OrReqFeat4 = 30;
    public static int REQSKILL = 31;
    public static int ReqSkillMinRanks = 32;  //ADDED - HOTU
    public static int REQSKILL2 = 33;         //ADDED - HOTU
    public static int ReqSkillMinRanks2 = 34; //ADDED - HOTU
    public static int Constant = 35;
    public static int TOOLSCATEGORIES = 36;
    public static int HostileFeat = 37;
    public static int MinLevel = 38;
    public static int MinLevelClass = 39;
    public static int MaxLevel = 40;
    public static int MinFortSave = 41;
    public static int PreReqEpic = 42;        //ADDED - HOTU
    
}


