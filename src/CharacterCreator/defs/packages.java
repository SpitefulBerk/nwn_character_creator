/*
 * classes.java
 *
 * Created on November 25, 2003, 2:04 AM
 */

package CharacterCreator.defs;

/**
 *
 * @author  James
 */
public class packages {
    public packages() {
    }
    
    public static int Label = 1;
    public static int Name = 2;
    public static int Description = 3;
    public static int ClassID = 4;
    public static int Attribute = 5;
    public static int Gold = 6;
    public static int School = 7;
    public static int Domain1 = 8;
    public static int Domain2 = 9;
    public static int Associate = 10;
    public static int SpellPref2DA = 11;
    public static int FeatPref2DA = 12;
    public static int SkillPref2DA = 13;
    public static int Equip2DA = 14;
    public static int Soundset = 15;
    public static int PlayerClass = 16; //ADDED IN HotU
 
}


