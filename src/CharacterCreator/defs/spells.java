/*
 * classes.java
 *
 * Created on November 25, 2003, 2:04 AM
 */

package CharacterCreator.defs;

/**
 *
 * @author  James
 */
public class spells {
    public spells() {
    }
    
    public static int Label = 1;
    public static int Name = 2;
    public static int IconResRef = 3;
    public static int School = 4;
    public static int Range = 5;
    public static int VS = 6;
    public static int MetaMagic = 7;
    public static int TargetType = 8;
    public static int ImpactScript = 9;
    public static int Bard = 10;
    public static int Cleric = 11;
    public static int Druid = 12; 
    public static int Paladin = 13;
    public static int Ranger = 14;
    public static int Wiz_Sorc = 15;
    public static int Innate = 16;
    public static int ConjTime = 17;
    public static int ConjAnim = 18;
    public static int ConjHeadVisual = 19;
    public static int ConjHandVisual = 20;
    public static int ConjGrndVisual = 21;
    public static int ConjSoundVFX = 22;
    public static int ConjSoundMale = 23;
    public static int ConjSoundFemale = 24;
    public static int CastAnim = 25;
    public static int CastTime = 26;
    public static int CastHeadVisual = 27;
    public static int CastHandVisual = 28;
    public static int CastGrndVisual = 29;
    public static int CastSound = 30;
    public static int Proj = 31;
    public static int ProjModel = 32;
    public static int ProjType = 33;
    public static int ProjSpwnPoint = 34;
    public static int ProjSound = 35;
    public static int ProjOrientation = 36;
    public static int ImmunityType = 37;
    public static int ItemImmunity = 38;
    public static int SubRadSpell1 = 39;
    public static int SubRadSpell2 = 40;
    public static int SubRadSpell3 = 41;
    public static int SubRadSpell4 = 42;
    public static int SubRadSpell5 = 43;
    public static int Category = 44;
    public static int Master = 45;
    public static int UserType = 46;
    public static int SpellDesc = 47;
    public static int UseConcentration = 48;
    public static int SpontaneousCast = 49;
    public static int AltMessage = 50;
    public static int HostileSetting = 51;
    public static int FeatID = 52; 
    public static int Counter1 = 53;
    public static int Counter2 = 54; 
    public static int HasProjectile = 55; //ADDED - HOTU
    
    
}


