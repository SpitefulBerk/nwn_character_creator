/*
 * classes.java
 *
 * Created on November 25, 2003, 2:04 AM
 */

package CharacterCreator.defs;

/**
 *
 * @author  James
 */
public class app {
    public app() {
    }
    
    public static int LABEL = 1;
    public static int STRING_REF = 2;
    public static int NAME = 3;
    public static int RACE = 4;
    public static int ENVMAP = 5;
    public static int BLOODCOLR = 6;
    public static int MODELTYPE = 7;
    public static int WEAPONSCALE = 8;
    public static int WING_TAIL_SCALE = 9;
    public static int HELMET_SCALE_M = 10;
    public static int HELMET_SCALE_F = 11;
    public static int MOVERATE = 12; 
    public static int WALKDIST = 13;
    public static int RUNDIST = 14;
    public static int PERSPACE = 15;
    public static int CREPERSPACE = 16;
    public static int HEIGHT = 17;
    public static int HITDIST = 18;
    public static int PREFATCKDIST = 19;
    public static int TARGETHEIGHT = 20;
    public static int ABORTONPARRY = 21;
    public static int RACIALTYPE = 22;
    public static int HASLEGS = 23;
    public static int HASARMS = 24;
    public static int PORTRAIT = 25;
    public static int SIZECATEGORY = 26;
    public static int PERCEPTIONDIST = 27;
    public static int FOOTSTEPTYPE = 28;
    public static int SOUNDAPPTYPE = 29;
    public static int HEADTRACK = 30;
    public static int HEAD_ARC_H = 31;
    public static int HEAD_ARC_V = 32;
    public static int HEAD_NAME = 33;
    public static int BODY_BAG = 34;
    public static int TARGETABLE = 35;
}


