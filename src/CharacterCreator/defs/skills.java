/*
 * classes.java
 *
 * Created on November 25, 2003, 2:04 AM
 */

package CharacterCreator.defs;

/**
 *
 * @author  James
 */
public class skills {
    public skills() {
    }
    
    public static int Label = 1;
    public static int Name = 2;
    public static int Description = 3;
    public static int Icon = 4;
    public static int Untrained = 5;
    public static int KeyAbility = 6;
    public static int ArmorCheckPenalty = 7;
    public static int AllClassesCanUse = 8;
    public static int Category = 9;
    public static int MaxCR = 10;
    public static int Constant = 11;
    public static int HostileSkill = 12;
 
}


