/*
 * CreditsMenu.java
 *
 * Created on April 4, 2003, 5:33 PM
 */

package CharacterCreator;

import java.awt.*;
/**
 *
 * @author  James
 */
public class CreditsMenu extends javax.swing.JFrame {

    /** Creates new form CreditsMenu */
    public CreditsMenu() {
        initComponents();

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        if ( (screenSize.getWidth() > getContentPane().getWidth()) && (screenSize.getHeight() > getContentPane().getHeight())) {
            int intwidth = new Double(((screenSize.getWidth()-getContentPane().getWidth())/2)).intValue();
            int intheight = new Double(((screenSize.getHeight()-getContentPane().getHeight())/2)).intValue();
            setLocation(intwidth, intheight);
        } else {
            setLocation(0,0);
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    private void initComponents() {//GEN-BEGIN:initComponents
        java.awt.GridBagConstraints gridBagConstraints;

        jScrollPane1 = new javax.swing.JScrollPane();
        CreditText = new javax.swing.JTextArea();
        OKButton = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();

        getContentPane().setLayout(new java.awt.GridBagLayout());

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                exitForm(evt);
            }
        });

        jScrollPane1.setHorizontalScrollBarPolicy(javax.swing.JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        jScrollPane1.setVerticalScrollBarPolicy(javax.swing.JScrollPane.VERTICAL_SCROLLBAR_ALWAYS );
        jScrollPane1.setPreferredSize(new java.awt.Dimension(350, 303));
        CreditText.setBackground(new java.awt.Color(0, 0, 0));
        CreditText.setEditable(false);
        CreditText.setForeground(new java.awt.Color(240, 216, 130));
        CreditText.setLineWrap(true);
        CreditText.setWrapStyleWord(true);
        CreditText.setText(
				  "PRC: v1.6\n"
				+ "Added Audio Support for Voice Selection.\n"
				+ "Added RAR Format Distribution.\n"
				+ "Made Simple Installer for Win32 Platforms.\n\n"
				+ "PRC: v1.5\n"
				+ "Improved compatibility with GNU/Linux and MacOS systems.\n\n"
			    + "PRC: v1.4\n"
			    + "Created a new release after making a new splash screen "
				+ "and changing the "
			    + "programs color scheme.\n\n"
			    + "PRC: v1.3\n"
				+ "This release was prepared by the "
				+ "Player Resource Consortium so we could make "
				+ "Level 40 Drow Samurai. \n\n"
				+ "PRC Website:\nhttp://nwnprc.netgamers.co.uk/\n\n"
				+ "CODI v1.1\n"
				+ "Programed by: James 'Halfelf' Stoneburner\n"
				+ "Contact: halfelf@city-of-doors.com\n\n"
				+ "This version was a team effort.  "
				+ "Thanks go to James, Grond and Garad Moonbeam for getting "
				+ "the updates done for SoU and 1.3x compatability as well as "
				+ "dynamic skill menus.\n\n"
				+ "Thanks go to Papermonk, first of all, "
				+ "for possessing the intestinal fortitude to stick with me "
				+ "on this project.  Many people said it couldn't be done, but "
				+ "here it is!\n\n"

				+ "Thanks also go to the whole CODI gang, "
				+ "specifically: Shkuey, Snowmit, Smeazel, Jupp, batinthehat, "
				+ "MacIntyre, and Permagrin. \n\n"

				+ "Thanks also to the few external alpha "
				+ "testers I had, and everyone else who supported this "
				+ "project.  Specific thanks go to GL (rabbithail) for "
				+ "helping me hash out ELC issues with the program, and "
				+ "sticking with me.  Thanks also go to Torlack for having "
				+ "such an incredible information source; Maximus at the "
				+ "NWVault for supporting CODI so much; RedR for hosting our "
				+ "chat room, and of course, Bioware for making such a great "
				+ "game.\n\nHappy custom character creating!");
		CreditText.append("\n");
        CreditText.setMargin(new java.awt.Insets(10, 10, 10, 10));
        //CreditText.setMinimumSize(new java.awt.Dimension(360, 244));
        //CreditText.setPreferredSize(new java.awt.Dimension(300, 300));
        CreditText.setCaretPosition(0);
        jScrollPane1.setViewportView(CreditText);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        getContentPane().add(jScrollPane1, gridBagConstraints);

        OKButton.setText("OK");
        OKButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OKButtonActionPerformed(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        getContentPane().add(OKButton, gridBagConstraints);

        getContentPane().add(jPanel1, new java.awt.GridBagConstraints());

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        getContentPane().add(jPanel2, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        getContentPane().add(jPanel3, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        getContentPane().add(jPanel4, gridBagConstraints);

        getContentPane().add(jPanel5, new java.awt.GridBagConstraints());

        pack();
    }//GEN-END:initComponents

    private void OKButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OKButtonActionPerformed
        // Add your handling code here:
        setVisible(false);
        dispose();
    }//GEN-LAST:event_OKButtonActionPerformed

    /** Exit the Application */
    private void exitForm(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_exitForm
        setVisible(false);
        dispose();
    }//GEN-LAST:event_exitForm



    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JButton OKButton;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextArea CreditText;
    // End of variables declaration//GEN-END:variables

}
