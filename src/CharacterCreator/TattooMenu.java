/*
 * TattooMenu.java
 *
 * Created on April 3, 2003, 12:19 AM
 */

package CharacterCreator;

import java.util.HashMap;
import java.awt.*;
/**
 *
 * @author  James
 */
public class TattooMenu extends javax.swing.JFrame {
    
    /** Creates new form TattooMenu */
    public TattooMenu() {
        initComponents();
        
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        if ( (screenSize.getWidth() > getContentPane().getWidth()) && (screenSize.getHeight() > getContentPane().getHeight())) {
            int intwidth = new Double(((screenSize.getWidth()-getContentPane().getWidth())/2)).intValue();
            int intheight = new Double(((screenSize.getHeight()-getContentPane().getHeight())/2)).intValue();            
            setLocation(intwidth, intheight);
        } else {
            setLocation(0,0);
        }        
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    private void initComponents() {//GEN-BEGIN:initComponents
        java.awt.GridBagConstraints gridBagConstraints;

        ChestCombo = new javax.swing.JComboBox();
        RightBicepCombo = new javax.swing.JComboBox();
        LeftBicepCombo = new javax.swing.JComboBox();
        RightForearmCombo = new javax.swing.JComboBox();
        LeftForearmCombo = new javax.swing.JComboBox();
        RightThighCombo = new javax.swing.JComboBox();
        LeftThighCombo = new javax.swing.JComboBox();
        RightShinCombo = new javax.swing.JComboBox();
        LeftShinCombo = new javax.swing.JComboBox();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        ChestLabel = new javax.swing.JLabel();
        LeftBicepLabel = new javax.swing.JLabel();
        RightBicepLabel = new javax.swing.JLabel();
        LeftForearmLabel = new javax.swing.JLabel();
        RightForearmLabel = new javax.swing.JLabel();
        LeftThighLabel = new javax.swing.JLabel();
        RightThighLabel = new javax.swing.JLabel();
        LeftShinLabel = new javax.swing.JLabel();
        RightShinLabel = new javax.swing.JLabel();
        OKButton = new javax.swing.JButton();
        CancelButton = new javax.swing.JButton();
        ResetButton = new javax.swing.JButton();

        getContentPane().setLayout(new java.awt.GridBagLayout());

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                exitForm(evt);
            }
        });

        ChestCombo.setFont(new java.awt.Font("Trebuchet MS", 0, 12));
        ChestCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "None", "Normal", "Tattooed" }));
        ChestCombo.setSelectedIndex(1);
        ChestCombo.setPreferredSize(new java.awt.Dimension(120, 25));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        getContentPane().add(ChestCombo, gridBagConstraints);

        RightBicepCombo.setFont(new java.awt.Font("Trebuchet MS", 0, 12));
        RightBicepCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "None", "Normal", "Tattooed" }));
        RightBicepCombo.setSelectedIndex(1);
        RightBicepCombo.setPreferredSize(new java.awt.Dimension(120, 25));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        getContentPane().add(RightBicepCombo, gridBagConstraints);

        LeftBicepCombo.setFont(new java.awt.Font("Trebuchet MS", 0, 12));
        LeftBicepCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "None", "Normal", "Tattooed" }));
        LeftBicepCombo.setSelectedIndex(1);
        LeftBicepCombo.setPreferredSize(new java.awt.Dimension(120, 25));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        getContentPane().add(LeftBicepCombo, gridBagConstraints);

        RightForearmCombo.setFont(new java.awt.Font("Trebuchet MS", 0, 12));
        RightForearmCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "None", "Normal", "Tattooed" }));
        RightForearmCombo.setSelectedIndex(1);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        getContentPane().add(RightForearmCombo, gridBagConstraints);

        LeftForearmCombo.setFont(new java.awt.Font("Trebuchet MS", 0, 12));
        LeftForearmCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "None", "Normal", "Tattooed" }));
        LeftForearmCombo.setSelectedIndex(1);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        getContentPane().add(LeftForearmCombo, gridBagConstraints);

        RightThighCombo.setFont(new java.awt.Font("Trebuchet MS", 0, 12));
        RightThighCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "None", "Normal", "Tattooed" }));
        RightThighCombo.setSelectedIndex(1);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 11;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        getContentPane().add(RightThighCombo, gridBagConstraints);

        LeftThighCombo.setFont(new java.awt.Font("Trebuchet MS", 0, 12));
        LeftThighCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "None", "Normal", "Tattooed" }));
        LeftThighCombo.setSelectedIndex(1);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 11;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        getContentPane().add(LeftThighCombo, gridBagConstraints);

        RightShinCombo.setFont(new java.awt.Font("Trebuchet MS", 0, 12));
        RightShinCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "None", "Normal", "Tattooed" }));
        RightShinCombo.setSelectedIndex(1);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 14;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        getContentPane().add(RightShinCombo, gridBagConstraints);

        LeftShinCombo.setFont(new java.awt.Font("Trebuchet MS", 0, 12));
        LeftShinCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "None", "Normal", "Tattooed" }));
        LeftShinCombo.setSelectedIndex(1);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 14;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        getContentPane().add(LeftShinCombo, gridBagConstraints);

        getContentPane().add(jPanel1, new java.awt.GridBagConstraints());

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        getContentPane().add(jPanel2, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        getContentPane().add(jPanel3, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 9;
        getContentPane().add(jPanel4, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 12;
        getContentPane().add(jPanel5, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 17;
        getContentPane().add(jPanel6, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 15;
        getContentPane().add(jPanel7, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        getContentPane().add(jPanel8, gridBagConstraints);

        getContentPane().add(jPanel9, new java.awt.GridBagConstraints());

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        getContentPane().add(jPanel10, gridBagConstraints);

        ChestLabel.setFont(new java.awt.Font("Trebuchet MS", 0, 10));
        ChestLabel.setText("Chest");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        getContentPane().add(ChestLabel, gridBagConstraints);

        LeftBicepLabel.setFont(new java.awt.Font("Trebuchet MS", 0, 10));
        LeftBicepLabel.setText("Left Bicep");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        getContentPane().add(LeftBicepLabel, gridBagConstraints);

        RightBicepLabel.setFont(new java.awt.Font("Trebuchet MS", 0, 10));
        RightBicepLabel.setText("Right Bicep");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        getContentPane().add(RightBicepLabel, gridBagConstraints);

        LeftForearmLabel.setFont(new java.awt.Font("Trebuchet MS", 0, 10));
        LeftForearmLabel.setText("Left Forearm");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        getContentPane().add(LeftForearmLabel, gridBagConstraints);

        RightForearmLabel.setFont(new java.awt.Font("Trebuchet MS", 0, 10));
        RightForearmLabel.setText("Right Forearm");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        getContentPane().add(RightForearmLabel, gridBagConstraints);

        LeftThighLabel.setFont(new java.awt.Font("Trebuchet MS", 0, 10));
        LeftThighLabel.setText("Left Thigh");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        getContentPane().add(LeftThighLabel, gridBagConstraints);

        RightThighLabel.setFont(new java.awt.Font("Trebuchet MS", 0, 10));
        RightThighLabel.setText("Right Thigh");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        getContentPane().add(RightThighLabel, gridBagConstraints);

        LeftShinLabel.setFont(new java.awt.Font("Trebuchet MS", 0, 10));
        LeftShinLabel.setText("Left Shin");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 13;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        getContentPane().add(LeftShinLabel, gridBagConstraints);

        RightShinLabel.setFont(new java.awt.Font("Trebuchet MS", 0, 10));
        RightShinLabel.setText("Right Shin");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 13;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        getContentPane().add(RightShinLabel, gridBagConstraints);

        OKButton.setText("OK");
        OKButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OKButtonActionPerformed(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 16;
        getContentPane().add(OKButton, gridBagConstraints);

        CancelButton.setText("Cancel");
        CancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CancelButtonActionPerformed(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 16;
        getContentPane().add(CancelButton, gridBagConstraints);

        ResetButton.setText("Reset");
        ResetButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ResetButtonActionPerformed(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 16;
        getContentPane().add(ResetButton, gridBagConstraints);

        pack();
    }//GEN-END:initComponents

    private void CancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CancelButtonActionPerformed
        // Add your handling code here:
        setVisible(false);
        dispose();        
    }//GEN-LAST:event_CancelButtonActionPerformed

    private void ResetButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ResetButtonActionPerformed
        // Add your handling code here:
        LeftThighCombo.setSelectedIndex(1);
        LeftForearmCombo.setSelectedIndex(1);
        RightThighCombo.setSelectedIndex(1);
        ChestCombo.setSelectedIndex(1);
        RightBicepCombo.setSelectedIndex(1);
        RightShinCombo.setSelectedIndex(1);
        RightForearmCombo.setSelectedIndex(1);
        LeftShinCombo.setSelectedIndex(1);
        LeftBicepCombo.setSelectedIndex(1);
    }//GEN-LAST:event_ResetButtonActionPerformed

    private void OKButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OKButtonActionPerformed
        // Add your handling code here:
        CreateMenu menucreate = TLKFactory.getCreateMenu();
        menucreate.MainCharData[17].put(new Integer(0), new Integer(ChestCombo.getSelectedIndex()));
        menucreate.MainCharData[17].put(new Integer(1), new Integer(LeftBicepCombo.getSelectedIndex()));
        menucreate.MainCharData[17].put(new Integer(2), new Integer(RightBicepCombo.getSelectedIndex()));
        menucreate.MainCharData[17].put(new Integer(3), new Integer(LeftForearmCombo.getSelectedIndex()));        
        menucreate.MainCharData[17].put(new Integer(4), new Integer(RightForearmCombo.getSelectedIndex()));
        menucreate.MainCharData[17].put(new Integer(5), new Integer(LeftThighCombo.getSelectedIndex()));        
        menucreate.MainCharData[17].put(new Integer(6), new Integer(RightThighCombo.getSelectedIndex()));
        menucreate.MainCharData[17].put(new Integer(7), new Integer(LeftShinCombo.getSelectedIndex()));
        menucreate.MainCharData[17].put(new Integer(8), new Integer(RightShinCombo.getSelectedIndex()));
        setVisible(false);
        dispose();        
    }//GEN-LAST:event_OKButtonActionPerformed
    
    /** Exit the Application */
    private void exitForm(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_exitForm
        setVisible(false);
        dispose();
    }//GEN-LAST:event_exitForm
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel4;
    private javax.swing.JComboBox LeftThighCombo;
    private javax.swing.JComboBox LeftForearmCombo;
    private javax.swing.JLabel RightBicepLabel;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JLabel RightShinLabel;
    private javax.swing.JComboBox RightThighCombo;
    private javax.swing.JLabel LeftShinLabel;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JComboBox LeftBicepCombo;
    private javax.swing.JLabel LeftThighLabel;
    private javax.swing.JButton OKButton;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JLabel RightForearmLabel;
    private javax.swing.JLabel LeftBicepLabel;
    private javax.swing.JComboBox ChestCombo;
    private javax.swing.JButton CancelButton;
    private javax.swing.JLabel RightThighLabel;
    private javax.swing.JComboBox RightBicepCombo;
    private javax.swing.JComboBox RightShinCombo;
    private javax.swing.JComboBox RightForearmCombo;
    private javax.swing.JButton ResetButton;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JLabel LeftForearmLabel;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JLabel ChestLabel;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JComboBox LeftShinCombo;
    // End of variables declaration//GEN-END:variables
    
}
